package com.zheng.stream.test;


import com.zheng.stream.model.User;
import com.zheng.stream.util.RedisStreamUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * 在spring初始化时执行，定时发送消息到stream中，用于模拟发送消息
 */
//@Component
public class StreamMessageRunner implements ApplicationRunner {

    @Resource
    private RedisStreamUtil redisStreamUtil;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(2);

        AtomicInteger integer = new AtomicInteger(0);
        //使用延时队列线程池模拟发送数据消息
        pool.scheduleAtFixedRate(()->{
            User zhangsan = new User("zhangsan"+integer.get(), 1 + integer.get());
            RecordId recordId = redisStreamUtil.add(redisStreamUtil.getStreamKey001(), zhangsan);
            integer.getAndIncrement();
            redisStreamUtil.delField(redisStreamUtil.getStreamKey001(),recordId);
            if (integer.get()>10){
                System.out.println("---------退出发送消息--------");
                pool.shutdown();
            }
        },5,3, TimeUnit.SECONDS);
    }
}
