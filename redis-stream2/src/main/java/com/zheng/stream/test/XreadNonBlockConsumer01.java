package com.zheng.stream.test;


import com.zheng.stream.model.User;
import com.zheng.stream.util.RedisStreamUtil;
import javafx.fxml.Initializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.connection.stream.StreamReadOptions;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * 独立消费者---可以读取到该key的全部消息
 */
//@Component
@Slf4j
public class XreadNonBlockConsumer01 implements InitializingBean, DisposableBean {
    private ThreadPoolExecutor threadPoolExecutor;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    private volatile boolean stop = false;


    /**
     * 初始化bean时执行，以轮询的方式主动去stream的指定key里读取消息
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // 初始化线程池
        threadPoolExecutor = new ThreadPoolExecutor(3, 5, 0, TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(), r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            thread.setName("xread-nonblock-01");
            return thread;
        });

        StreamReadOptions options = StreamReadOptions.empty()
//                如果没有数据，则阻塞1s，阻塞时间需要小于·spring.redis.timeout·
                .block(Duration.ofMillis(1000))
//                一直阻塞直到获取数据，可能会报超时异常
//                .block(Duration.ofMillis(0))
//                一次获取10条数据
                .count(10);

        StringBuilder readBuilder = new StringBuilder("0-0");

        threadPoolExecutor.execute(()->{
            while (!stop){
                //主动到redis的stream中去读取，options设置了每读取一次阻塞一秒
                List<ObjectRecord<String, User>> objectRecords = redisTemplate.opsForStream()
                        .read(User.class, options,
                                StreamOffset.create(RedisStreamUtil.STREAM_KEY_001, ReadOffset.from(readBuilder.toString())));

                if (CollectionUtils.isEmpty(objectRecords)){
                    log.warn("没有读取到数据");
                    continue;
                }
                objectRecords.stream().forEach(objectRecord->{
                    log.info("获取到的数据信息 id:[{}] book:[{}]", objectRecord.getId(), objectRecord.getValue());
                    readBuilder.setLength(0);
                    readBuilder.append(objectRecord.getId());

                });
            }
        });


    }

    /**
     * 在销毁bean时把线程池关闭
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        stop = true;
        threadPoolExecutor.shutdown();
        threadPoolExecutor.awaitTermination(3,TimeUnit.SECONDS);
    }

}
