package com.zheng.stream.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ErrorHandler;

@Slf4j
public class CustomErrorHandler implements ErrorHandler {
    @Override
    public void handleError(Throwable throwable) {
        log.error("发生了异常",throwable);
    }
}
