package com.zheng.stream.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamRecords;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class RedisStreamUtil {
    public static final String STREAM_KEY_001 = "stream-001";
    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    /**
     * 添加记录到流中
     * @param streamKey
     * @param t
     * @param <T>
     */
    public <T> RecordId add(String streamKey,T t){
        ObjectRecord<String, T> record = StreamRecords.newRecord()
                .in(streamKey)  //key
                .ofObject(t) //消息数据
                .withId(RecordId.autoGenerate());
//        发送消息
        RecordId recordId = redisTemplate.opsForStream().add(record);

        log.info("添加成功，返回的record-id[{}]",recordId);


        return recordId;
    }

    /**
     * 用来创建绑定流和组
     */
    public void addGroup(String key, String groupName){
        redisTemplate.opsForStream().createGroup(key,groupName);
    }
    /**
     * 用来判断key是否存在
     */
    public boolean hasKey(String key){
        if(key==null){
            return false;
        }else{
            return redisTemplate.hasKey(key);
        }

    }
    /**
     * 用来删除掉消费了的消息
     */
    public void delField(String key,RecordId recordIds){
        redisTemplate.opsForStream().delete(key,recordIds);
    }

    /**
     * 用来初始化 实现绑定key和消费组
     */
    public void initStream(String key, String group){
        //判断key是否存在，如果不存在则创建
        boolean hasKey = hasKey(key);
        if(!hasKey){
            Map<String,Object> map = new HashMap<>();
            map.put("key","value");
            RecordId recordId = add(key, map);
            addGroup(key,group);   //第一次初始化时需要把Stream和group绑定，
            delField(key,recordId);  //清除掉该条无用数据
            log.info("stream:{}-group:{} initialize success",key,group);
        }
    }


    public String getStreamKey001(){
        return STREAM_KEY_001;
    }
}
