package com.zheng.stream.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Book extends User{
    private String title;
    private String author;
}
