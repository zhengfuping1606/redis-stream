package com.zheng.stream.controller;

import com.zheng.stream.model.User;
import com.zheng.stream.model.Book;
import com.zheng.stream.util.RedisStreamUtil;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/index")
public class index {
    @Resource
    private RedisStreamUtil redisStreamUtil;


    @GetMapping("/login")
    public String login(User user){

        RecordId recordId = redisStreamUtil.add(redisStreamUtil.getStreamKey001(), user);
        redisStreamUtil.delField(redisStreamUtil.getStreamKey001(),recordId);

        return "成功！";
    }
    @GetMapping("/login2")
    public String login(Book book){

        RecordId recordId = redisStreamUtil.add(redisStreamUtil.getStreamKey001(), book);

        redisStreamUtil.delField(redisStreamUtil.getStreamKey001(),recordId);
        return "成功！";
    }
}
